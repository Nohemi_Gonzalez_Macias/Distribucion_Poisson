$(document).ready(function(){
  // así redirecciona:
  //window.location.replace("login.html");

var factorial = function factorial (x) {
  var total = 1;
  for (i=1; i<=x; i++) {
    total = total * i;
  }
  return total;
}

$("#calcular").click(function(){
  var lambda = (document).getElementById("lambda").value;
  var t = (document).getElementById("t").value;
  var x = (document).getElementById("x").value;
  var e = Math.exp(1);

  ex = Math.pow(e, (-lambda*t));
  ex = ex * Math.pow((lambda*t), x);
  var venneno = ex / factorial(x);
  
  $(".result").replaceWith("<div class='resu'><p>Resultado: " + venneno.toFixed(4) + "</p></div>");
});

$("#limpiar").click(function(){
  lamda = 0;
  t = 0;
  x = 0;
  e = 0;
  ex = 0;
  $(".resu").remove();
  $("#lambda").val("");
  $("#t").val("");
  $("#x").val("");
  $("#e").val("");
});

});
